package com.simplepvptdm;

import com.simplepvp.Game;
import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.Team;

import java.util.*;

public class TDMGame extends Game {

    // The variables needed for the game.
    private int redTeamKillCount = 0;
    private int blueTeamKillCount = 0;
    private ItemStack joinRedTeamItem;
    private ItemStack joinBlueTeamItem;
    private BossBar redKillBar;
    private BossBar blueKillBar;
    private final Team redTeam;
    private final Team blueTeam;
    private final List<Player> redTeamPlayerQueue = new ArrayList<>();
    private final List<Player> blueTeamPlayerQueue = new ArrayList<>();
    private final HashSet<Player> redTeamPlayers = new HashSet<>();
    private final HashSet<Player> blueTeamPlayers = new HashSet<>();
    private final Random random = new Random(System.currentTimeMillis());

    // Initialise the game.
    public TDMGame(String gameName, Material gameIcon, TDMMapConfig mapConfig) {
        super(gameName, gameIcon, mapConfig); // Call the super.
        generateTeamJoinItems();
        setupBossBars();
        redTeam = createRedTeam();
        blueTeam = createBlueTeam();
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Setup Methods
----------------------------------------------------------------------------------------------------------------------*/

    private void setupBossBars() {
        BarStyle barStyle;
        switch (SimplePVPTDM.getTeamKills()) {
            case 6 -> barStyle = BarStyle.SEGMENTED_6;
            case 10 -> barStyle = BarStyle.SEGMENTED_10;
            case 12 -> barStyle = BarStyle.SEGMENTED_12;
            case 20 -> barStyle = BarStyle.SEGMENTED_20;
            default -> barStyle = BarStyle.SOLID;
        }
        redKillBar = Bukkit.createBossBar(null, BarColor.RED, barStyle);
        blueKillBar = Bukkit.createBossBar(null, BarColor.BLUE, barStyle);

        redKillBar.setProgress(0);
        redKillBar.setVisible(true);
        blueKillBar.setProgress(0);
        blueKillBar.setVisible(true);
    }

    private void generateTeamJoinItems() {
        ItemStack redItem = new ItemStack(Material.RED_CONCRETE);
        ItemMeta redItemMeta = redItem.getItemMeta();
        assert redItemMeta != null;
        redItemMeta.setDisplayName(ChatColor.RED + "Join Red Team");
        redItem.setItemMeta(redItemMeta);
        joinRedTeamItem = redItem;

        ItemStack blueItem = new ItemStack(Material.BLUE_CONCRETE);
        ItemMeta blueItemMeta = blueItem.getItemMeta();
        assert blueItemMeta != null;
        blueItemMeta.setDisplayName(ChatColor.BLUE + "Join Blue Team");
        blueItem.setItemMeta(blueItemMeta);
        joinBlueTeamItem = blueItem;
    }

    private Team createRedTeam() {
        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getTeam("TDM_RED_TEAM");

        if (team == null) {
            team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().registerNewTeam("TDM_RED_TEAM");
        }

        team.setColor(ChatColor.RED);

        return team;
    }

    private Team createBlueTeam() {
        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getTeam("TDM_BLUE_TEAM");

        if (team == null) {
            team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().registerNewTeam("TDM_BLUE_TEAM");
        }

        team.setColor(ChatColor.BLUE);

        return team;
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                  Overrided Methods
----------------------------------------------------------------------------------------------------------------------*/

    @Override
    public boolean sendPlayer(Player player) {
        if (super.sendPlayer(player)) {
            player.getInventory().addItem(SimplePVPTDM.getKits().getKitMenuItem());
            player.getInventory().setItem(7, joinRedTeamItem);
            player.getInventory().setItem(8, joinBlueTeamItem);
            return true;
        }
        return false;
    }

    @Override
    public boolean removePlayer(Player player) {
        // Only run if the player was removed from the game.
        if (super.removePlayer(player)) {
            redTeamPlayers.remove(player);
            blueTeamPlayers.remove(player);

            redTeamPlayerQueue.remove(player);
            blueTeamPlayerQueue.remove(player);

            redKillBar.removePlayer(player);
            blueKillBar.removePlayer(player);

            redTeam.removeEntry(player.getName());
            blueTeam.removeEntry(player.getName());

            checkGameWin();
            return true;
        }
        return false;
    }

    @Override
    protected void killPlayer(Player player, Player damager, EntityDamageEvent.DamageCause cause) {
        super.killPlayer(player, damager, cause);

        if (damager != null) {
            if (redTeamPlayers.contains(damager)) {
                redTeamKillCount++;
                redKillBar.setProgress((double) redTeamKillCount / SimplePVPTDM.getTeamKills());
            } else if (blueTeamPlayers.contains(damager)) {
                blueTeamKillCount++;
                blueKillBar.setProgress((double) blueTeamKillCount / SimplePVPTDM.getTeamKills());
            }
        }

        if (redTeamKillCount < SimplePVPTDM.getTeamKills() && blueTeamKillCount < SimplePVPTDM.getTeamKills()) {
            player.sendTitle(ChatColor.RED + "You Died!", "You will respawn soon.", 0, 20, 10);
            respawnPlayer(player);
        }

        checkGameWin();
    }

    @Override
    protected void respawnPlayer(Player player) {
        Bukkit.getScheduler().runTaskLater(SimplePVPTDM.getPlugin(), () -> {
            if (gameStarted && !gameEnded) {
                if (redTeamPlayers.contains(player)) {
                    player.teleport(((TDMMapConfig) getMapConfig()).getRedTeamSpawn());
                } else {
                    player.teleport(((TDMMapConfig) getMapConfig()).getBlueTeamSpawn());
                }
                player.setGameMode(GameMode.SURVIVAL);
                SimplePVPTDM.getKits().equipKit(player);
            }
        }, 60);
    }

    @Override
    protected void startGame() {
        int halfTeamSize = getPlayers().size() / 2;

        if (redTeamPlayerQueue.size() > halfTeamSize) {
            redTeamPlayerQueue.subList(halfTeamSize, redTeamPlayerQueue.size()).clear();
        }

        if (blueTeamPlayerQueue.size() > halfTeamSize) {
            blueTeamPlayerQueue.subList(halfTeamSize, blueTeamPlayerQueue.size()).clear();
        }

        for (Player player : redTeamPlayerQueue) {
            redTeamPlayers.add(player);
            redTeam.addEntry(player.getName());
        }

        for (Player player : blueTeamPlayerQueue) {
            blueTeamPlayers.add(player);
            blueTeam.addEntry(player.getName());
        }

        redTeamPlayerQueue.clear();
        blueTeamPlayerQueue.clear();

        for (Player player : getPlayers()) {

            redKillBar.addPlayer(player);
            blueKillBar.addPlayer(player);

            if (!redTeamPlayers.contains(player) && !blueTeamPlayers.contains(player)) {

                if (redTeamPlayers.size() > blueTeamPlayers.size()) {
                    blueTeamPlayers.add(player);
                    blueTeam.addEntry(player.getName());
                } else if (redTeamPlayers.size() < blueTeamPlayers.size()) {
                    redTeamPlayers.add(player);
                    redTeam.addEntry(player.getName());
                } else {
                    if (random.nextBoolean()) {
                        blueTeamPlayers.add(player);
                        blueTeam.addEntry(player.getName());
                    } else {
                        redTeamPlayers.add(player);
                        redTeam.addEntry(player.getName());
                    }
                }
            }
        }


        // Teleport all the players and give them their items.
        for (Player player : getPlayers()) {

            if (redTeamPlayers.contains(player)) {
                player.teleport(((TDMMapConfig) getMapConfig()).getRedTeamSpawn());
            } else {
                player.teleport(((TDMMapConfig) getMapConfig()).getBlueTeamSpawn());
            }

            player.setGameMode(GameMode.SURVIVAL);
            SimplePVPTDM.getKits().equipKit(player);
        }
    }

    @Override
    protected void checkGameWin() {
        super.checkGameWin();
        if (gameStarted && !gameEnded) {
            if (redTeamKillCount >= SimplePVPTDM.getTeamKills() && blueTeamKillCount >= SimplePVPTDM.getTeamKills()) {
                endGame(WinState.TIE);
            } else if (redTeamKillCount >= SimplePVPTDM.getTeamKills() || blueTeamPlayers.isEmpty()) {
                endGame(WinState.RED);
            } else if (blueTeamKillCount >= SimplePVPTDM.getTeamKills() || redTeamPlayers.isEmpty()) {
                endGame(WinState.BLUE);
            }
        }
    }

    // End the game.
    private void endGame(WinState winState) {
        // Show the winner if
        switch (winState) {
            case RED -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.RED + "Red team" + ChatColor.GOLD + " won the game!");
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.GREEN + "You Win!", "", 0, 20, 10);
                SimpleUtil.sendPlayersTitle(blueTeamPlayers.stream().toList(), ChatColor.DARK_GRAY + "You Lost.", "", 0, 20, 10);
            }
            case BLUE -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.BLUE + "Blue team" + ChatColor.GOLD + " won the game!");
                SimpleUtil.sendPlayersTitle(blueTeamPlayers.stream().toList(), ChatColor.GREEN + "You Win!", "", 0, 20, 10);
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.DARK_GRAY + "You Lost.", "", 0, 20, 10);
            }
            case TIE -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + "The game was a tie!");
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.GOLD + "Tie.", "", 0, 20, 10);
            }
        }

        super.endGame();
    }

    @Override
    protected void resetGame() {
        super.resetGame();

        redTeamPlayers.clear();
        blueTeamPlayers.clear();
        redTeamPlayerQueue.clear();
        blueTeamPlayerQueue.clear();
        redTeamKillCount = 0;
        blueTeamKillCount = 0;
        redKillBar.setProgress(0);
        blueKillBar.setProgress(0);
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Methods
----------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Events
----------------------------------------------------------------------------------------------------------------------*/
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player player) {

            // Prevent friendly fire
            if (e.getDamager() instanceof Player damager) {
                if (redTeamPlayers.contains(player) && redTeamPlayers.contains(damager)) {
                    e.setCancelled(true);
                } else if (blueTeamPlayers.contains(player) && blueTeamPlayers.contains(damager)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (hasPlayer(e.getPlayer())) {
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                ItemStack item = e.getItem();
                if (item != null) {
                    if (item.equals(joinRedTeamItem)) {
                        if (redTeamPlayerQueue.contains(e.getPlayer())) {
                            e.getPlayer().sendMessage(ChatColor.DARK_RED + "You are already queued for this team.");
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 0);
                        } else {
                            e.getPlayer().sendMessage(ChatColor.GOLD + "Adding you to the " + ChatColor.RED + "red" + ChatColor.GOLD +" team queue.");
                            blueTeamPlayerQueue.remove(e.getPlayer());
                            redTeamPlayerQueue.add(e.getPlayer());
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 2);
                        }
                        e.setCancelled(true);
                    } else if (item.equals(joinBlueTeamItem)) {
                        if (blueTeamPlayerQueue.contains(e.getPlayer())) {
                            e.getPlayer().sendMessage(ChatColor.DARK_RED + "You are already queued for this team.");
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 0);
                        } else {
                            e.getPlayer().sendMessage(ChatColor.GOLD + "Adding you to the " + ChatColor.BLUE + "blue" + ChatColor.GOLD +" team queue.");
                            redTeamPlayerQueue.remove(e.getPlayer());
                            blueTeamPlayerQueue.add(e.getPlayer());
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 2);
                        }
                        e.setCancelled(true);
                    }
                }
            }
        }
    }
}

