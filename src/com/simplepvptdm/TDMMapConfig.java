package com.simplepvptdm;

import com.simplepvp.MapConfig;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class TDMMapConfig extends MapConfig {
    private final Location redTeamSpawn;
    private final Location blueTeamSpawn;

    public TDMMapConfig(ConfigurationSection cs) {
        // Call the super.
        super(convertToTitleCase(cs.getName().replace('-', ' ')),
                cs.getInt("min-players"), cs.getInt("max-players"), cs.getLocation("spawn-location"));

        redTeamSpawn = cs.getLocation("red-team-spawn");
        blueTeamSpawn = cs.getLocation("blue-team-spawn");

    }

    // Used to convert strings to title case. Used for map names.
    public static String convertToTitleCase(String text) {
        StringBuilder converted = new StringBuilder();

        boolean upperCaseNext = true;
        for (char c : text.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                upperCaseNext  = true;
            } else if (upperCaseNext) {
                c = Character.toUpperCase(c);
                upperCaseNext  = false;
            } else {
                c = Character.toLowerCase(c);
            }
            converted.append(c);
        }

        return converted.toString();
    }

    // Getters.
    public Location getRedTeamSpawn() {
        return redTeamSpawn;
    }

    public Location getBlueTeamSpawn() {
        return blueTeamSpawn;
    }
}
